import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import htLogo from "./assets/logo-ht-cs.svg";
import 'react-app-polyfill/ie11'
import 'react-app-polyfill/stable'

ReactDOM.render(
  <React.StrictMode>
    <App />
      <footer>
          <img src={htLogo} alt="logo"/>
          <div className="contacts">
              <address>426057, Россия, Удмуртская Республика, г. Ижевск, ул. Карла Маркса, 246 (ДК
                  «Металлург»)
              </address>
              <div className="phone">+7 (3412) 93-88-61, 43-29-29</div>
              <div className="site"><a href={'https://htc-cs.ru/'}>htc-cs.ru</a></div>
          </div>
      </footer>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
