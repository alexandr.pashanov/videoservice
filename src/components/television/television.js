import React from "react";
import './television.scss';

const Television = (props) => {
    return (
        <div className="television">
            {props.tv.map((channel, index) => {
                return (<div className={`${index === props.tv.length - 1 ? "" : "separator"}`} key={index}>
                    <div className="img">
                    <img src={channel.img} alt="logo"/>
                    </div>
                    <article className="program">
                        <div className="title"><h4><strong>{channel.name}</strong></h4></div>
                        {channel.program.map((show, index) => {
                            return <div className={`show ${index===0 ? "current" : ""}`} key={index}>
                                <time className="time">{show.time}</time>
                                <time className="time">{show.name}</time>
                            </div>
                        })}
                    </article>
                </div>

            )})
            }
        </div>
    )
};

export default Television;
