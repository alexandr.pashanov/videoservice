import React from "react";
import './../films/films.scss'
import hot from "../../assets/hot.svg";
import comedy from "../../assets/genre/comedy.svg";
import drama from "../../assets/genre/drama.svg";
import fiction from "../../assets/genre/fiction.svg";
import horror from "../../assets/genre/horror.svg";


const Films = (props) => {
    return (
        <div className="films">
            <section className="section">
                <title><h3><strong><img src={hot} alt="logo"/>Новинки</strong></h3></title>
                <div className="new-films">
                    {props.films.map((film, index) => {
                        return <article onClick={() => props.selectFilm(film)} key={index}
                                        className={`${index !== 0 ? "article-margin" : ""}`}>
                            <div className="film">
                                <picture><img src={film.img} alt="logo"/></picture>
                                <div className="description">{film.description}</div>
                            </div>
                            <div className="name">{film.name}</div>
                        </article>
                    })}
                </div>
            </section>
            <section className="section">
                <title><h3><strong>Жанры</strong></h3></title>
                <div className="genres">
                    <div className="genre comedy">
                        <img src={comedy} alt="logo"/>
                        <div>Комедии</div>
                    </div>
                    <div className="genre drama">
                        <img src={drama} alt="logo"/>
                        <div>Драмы</div>
                    </div>
                    <div className="genre fiction">
                        <img src={fiction} alt="logo"/>
                        <div>Фантастика</div>
                    </div>
                    <div className="genre horror">
                        <img src={horror} alt="logo"/>
                        <div>Ужасы</div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default Films;
