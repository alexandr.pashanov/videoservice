import React from "react";
import back from './../../assets/service/back-link.svg';
import del from './../../assets/service/delete.svg';
import './films-detail.scss'

const FilmsDetail = (props) => {

    let commentary;

    function onCommentaryChange(event) {
        commentary = event.target.value;
    }

    return (
        <section className="description-area">
            <div className={'description-controls'}>
                <picture><img onClick={() => props.return()} src={back} alt={'назад'}/></picture>
                <div className={'image-container'}>
                    <img className={'image-poster'} src={props.film.img} alt={'Постер'}/>
                </div>
                <div className={'film'}>
                    <div>
                        <p>Название:</p>
                        <p className={'film_name'}>{props.film.name}</p>
                    </div>
                    <div>
                        <p>Название:</p>
                        <p className={'film_country'}>{props.film.country}</p>
                    </div>
                    <div>
                        <p>Жанр:</p>
                        <p className={'film_genre'}>{props.film.genre}</p>
                    </div>
                    <div className={'film_description'}>
                        {props.film.description}
                    </div>
                </div>
            </div>
            <div className={'commentaries'}>
                <div className={'commentaries-title'}>Комментарии</div>
                {props.author &&
                    <div className={'commentaries-container add'}>
                        <div className={'commentaries-item add-commentary'}>
                            <textarea placeholder={'Введите комментарий'} onChange={(event) => onCommentaryChange(event)} rows={6} />
                            {   props.showError &&
                                <div>Коммментарий не может быть пустым</div>
                            }
                        </div>
                        <div className={'button-container red-button'}>
                            <input type='button' value={'Опубликовать'} onClick={() => props.publishCommentary(commentary)} />
                        </div>
                    </div>
                }
                {
                    props.film.commentaries.map((commentary, index) => {
                        console.log(props.film.commentaries)
                        return <div className={'commentaries-container'} key={index}>
                            <div className={'commentaries-item'}>
                                <div className={'author'}>{commentary.author}</div>
                                <div className={'message'}>{commentary.message}</div>
                            </div>
                            <div className={'button-container'}>
                                {props.author === commentary.author &&
                                <img alt={'Удалить комментарий'} src={del} onClick={() => props.removeCommentary(commentary)} />
                                }
                            </div>
                        </div>

                    })
                }
            </div>
        </section>
    )
};

export default FilmsDetail
