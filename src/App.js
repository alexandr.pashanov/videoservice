import React from 'react';
import logo from './assets/logo.svg';
import './App.scss';
import f1 from './assets/films/picture.svg';
import f2 from './assets/films/picture.png';
import f3 from './assets/films/picture(1).svg';
import f4 from './assets/films/picture(1).png';
import Television from "./components/television/television";
import Films from "./components/films/films";
import c1 from './assets/channels/first.svg';
import c2 from './assets/channels/2x2.svg';
import c3 from './assets/channels/amedia.svg';
import c4 from './assets/channels/rbc.svg';
import FilmsDetail from "./components/films/film-detail";

class App extends React.Component {
    userLogin;
    userPassword;
    inputEditCurrentUser;
    localStorageKeyUser = 'testAppReactUser';
    localStorageKeyFilm = 'testAppReactFilm';
    searchString;
    state = {};
    memorize;

    constructor(props) {
        super(props);
        const memorizedUser = localStorage.getItem(this.localStorageKeyUser);
        this.memorize = false;
        this.state = {
            showFilms: true,
            currentUser: memorizedUser ? memorizedUser : '',
            films: [
                {
                    id: 1,
                    name: 'Мульт в кино. Выпуск №103. Некогда грустить!',
                    img: f1,
                    description: 'В новом выпуске ми-ми-мишки изобретут машину сна, а Дракоша Тоша научит завязывать шнурки. Также зрители увидят новые серии мультфильмов «Четверо в кубе», «Лео и Тиг» и совершенно новый мультсериал «Снежная королева: Хранители чудес». «МУЛЬТ в кино. Выпуск 103. Некогда грустить!» — в кинотеатрах с 28 сентября!',
                    commentaries: [],
                    country: 'country',
                    genre: 'МУльтфильм'
                },
                {
                    id: 2,
                    name: 'Новый Бэтмен',
                    img: f3,
                    description: 'Американский телевизионный сериал 1960-х годов, основанный на комиксах компании DC. Основные персонажи сериала, Бэтмен и Робин — герои в масках, сражающиеся с преступностью в вымышленном городе Готэм-сити',
                    commentaries: [],
                    country: 'country',
                    genre: 'Фантастика'
                },
                {
                    id: 3,
                    name: 'Однажды... в Голливуде',
                    img: f2,
                    description: 'Фильм повествует о череде событий, произошедших в Голливуде в 1969 году, на закате его «золотого века». Известный актер Рик Далтон и его дублер Клифф Бут пытаются найти свое место в стремительно меняющемся мире киноиндустрии.',
                    commentaries: [],
                    country: 'country',
                    genre: 'Комедия'
                },
                {
                    id: 4,
                    name: 'Стриптизерши',
                    img: f4,
                    description: 'Танцовщицы элитного стриптиз-клуба, клиенты которого — известные финансисты с Уолл-Стрит — привыкли к большим заработкам и роскошной жизни. Но после финансового кризиса 2008 года посетителей в клубе заметно поубавилось, и деньги к девушкам уже не текут рекой. Тяжёлые времена требуют отчаянных мер, и бывшие танцовщицы решаются на авантюрный шаг.',
                    commentaries: [],
                    country: 'country',
                    genre: 'Драма'
                },
            ],
            foundFilms: [],
            tvChannels: [
                {
                    img: c1,
                    name: 'Первый канал',
                    program: [
                        {time: '13:00', name: 'Новости (с субтитрами)'},
                        {time: '14:00', name: 'Давай поженимся'},
                        {time: '15:00', name: 'Другие новости'},
                    ]
                },
                {
                    img: c2,
                    name: '2x2',
                    program: [
                        {time: '13:00', name: 'МУЛЬТ ТВ. Сезон 4, 7 серия'},
                        {time: '14:00', name: 'ПОДОЗРИТЕЛЬНАЯ СОВА. Сезон 7, 7 серия'},
                        {time: '15:00', name: 'БУРДАШЕВ. Сезон 1, 20 серия'},
                    ]
                },
                {
                    img: c3,
                    name: 'РБК',
                    program: [
                        {time: '13:00', name: 'ДЕНЬ. Горючая смесь: как бороться с суррогатом на АЗС'},
                        {time: '14:00', name: 'ДЕНЬ. Главные темы'},
                        {time: '15:00', name: 'Главные новости'},
                    ]
                },
                {
                    img: c4,
                    name: 'AMEDIA PREMIUM',
                    program: [
                        {time: '13:00', name: 'Клиент всегда мёртв'},
                        {time: '14:00', name: 'Голодные игры: Сойка-пересмешница. Часть I'},
                        {time: '15:00', name: 'Секс в большом городе'},
                    ]
                },
                {
                    img: c4,
                    name: 'AMEDIA PREMIUM',
                    program: [
                        {time: '13:00', name: 'Клиент всегда мёртв'},
                        {time: '14:00', name: 'Голодные игры: Сойка-пересмешница. Часть I'},
                        {time: '15:00', name: 'Секс в большом городе'},
                    ]
                }


            ],
            showLoginForm: false,
            showError: false,
            editUserName: false,
            selectedFilm: null,
            commentaryNotValid: false
        };
        this.state.films.forEach(film => {
            let key = this.localStorageKeyFilm + film.id;
            let foundCommentaries = JSON.parse(localStorage.getItem(key));
            if (foundCommentaries) {
                film.commentaries = film.commentaries.concat(foundCommentaries);
            }
            return film;
        })
    }


    showTv = () => {
        const currentState = {...this.state};
        currentState.showFilms = false;

        this.setState(currentState);
    };

    showFilms = () => {
        const currentState = {...this.state};
        currentState.showFilms = true;

        this.setState(currentState);
    };

    showLoginForm = () => {
        const currentState = {...this.state};
        currentState.showLoginForm = true;

        this.setState(currentState);
    };

    login = () => {
        const currentState = {...this.state};
        if (this.userLogin) {
            if (!this.userPassword || this.userPassword === '') {
                currentState.showError = true;
            } else {
                currentState.showLoginForm = false;
                currentState.showError = false;
                currentState.currentUser = this.userLogin;
                this.userLogin = undefined;
            }
            if (this.memorize) {
                localStorage.setItem(this.localStorageKeyUser, currentState.currentUser)
            }
            this.memorize = false;
        } else {
            currentState.showLoginForm = false;
        }
        this.setState(currentState);
    };

    logout = () => {
        const currentState = {...this.state};
        currentState.showLoginForm = false;
        currentState.showError = false;
        this.userLogin = undefined;
        currentState.currentUser = this.userLogin;
        currentState.memorize = false;
        const memorizedUser = localStorage.getItem(this.localStorageKeyUser);
        if (memorizedUser) {
            localStorage.removeItem(this.localStorageKeyUser);
        }

        this.setState(currentState);
    };

    loginInput = (event) => {
        this.userLogin = event.target.value;
    };

    passwordInput = (event) => {
        this.userPassword = event.target.value;
        if (event.target.value === '') {
            const currentState = {...this.state};
            currentState.showError = true;
            this.setState(currentState);
        }
    };

    showEditUserName = () => {
        const currentState = {...this.state};
        currentState.editUserName = true;
        this.inputEditCurrentUser = currentState.currentUser;
        this.setState(currentState);
    };

    saveNewUserName = (event) => {
        if (this.inputEditCurrentUser !== '') {
            const currentState = {...this.state};
            const memorizedUser = localStorage.getItem(this.localStorageKeyUser);
            if (memorizedUser && memorizedUser === this.state.currentUser) {
                localStorage.setItem(this.localStorageKeyUser, event.target.value);
            }
            currentState.editUserName = false;
            currentState.currentUser = event.target.value;
            if (this.memorize) {
                localStorage.setItem(this.localStorageKeyUser, currentState.currentUser);
            }
            this.setState(currentState);
        }
    };

    setMemorized = () => {
        this.memorize = !this.memorize;
    };

    searchStringInput = (event) => {
        this.searchString = event.target.value;
    };

    findElements = () => {
        const currentState = {...this.state};
        currentState.selectedFilm = null;
        if (this.searchString) {
            currentState.foundFilms = this.state.films.filter((film) => {
                return film.name.toLowerCase().includes(this.searchString.toLowerCase());
            });
        } else {
            currentState.foundFilms = [];
        }
        this.setState(currentState);
    };

    selectFilm = (film) => {
        const currentState = {...this.state};
        currentState.selectedFilm = film;
        this.setState(currentState);
    };

    addCommentary = (commentary) => {
        const currentState = {...this.state};
        if (!commentary) {
            currentState.commentaryNotValid = true;
        } else {
            currentState.commentaryNotValid = false;
            const foundFilm = currentState.films.find(film => {
                return film.id === currentState.selectedFilm.id;
            });
            foundFilm.commentaries.unshift({
                author: currentState.currentUser,
                message: commentary
            });
            localStorage.setItem(this.localStorageKeyFilm + foundFilm.id, JSON.stringify(foundFilm.commentaries));
        }
        this.setState(currentState);

    };

    removeCommentary = (commentary) => {
        const currentState = {...this.state};
        const foundFilm = currentState.films.find(film => {
            return film.id === currentState.selectedFilm.id;
        });
        foundFilm.commentaries = foundFilm.commentaries.filter(coment => {
            return coment !== commentary
        });
        this.setState(currentState);
        localStorage.setItem(this.localStorageKeyFilm + foundFilm.id, JSON.stringify(foundFilm.commentaries));
    };

    returnToFilms = () => {
        const currentState = {...this.state};
        currentState.selectedFilm = null;
        this.setState(currentState);
    };

    render() {

        const currentScreen = this.state.showFilms
            ? <Films selectFilm={this.selectFilm}
                     films={this.state.foundFilms.length > 0 ? this.state.foundFilms : this.state.films}/>
            : <Television tv={this.state.tvChannels}/>;

        return (
            <div className="App">
                <header>
                    <div className="App-logo">
                        <img src={logo} alt="logo"/>
                        <p>Видеосервис</p>
                    </div>
                    <div className="search-field">
                        <input type="text" placeholder="Поиск..." onChange={(event) => this.searchStringInput(event)}/>
                        <input type="button" className="button-white" onClick={(event) => this.findElements(event)}
                               value="Найти"/>
                    </div>
                    {
                        !this.state.currentUser &&
                        <div className="red-button" onClick={() => this.showLoginForm()}>
                            <input type="button" value="Войти"/>
                        </div>}
                    {
                        this.state.currentUser &&
                        <div className="user">
                            {
                                !this.state.editUserName &&
                                <div onClick={() => this.showEditUserName()}>{this.state.currentUser}</div>
                            }
                            {
                                this.state.editUserName &&
                                <input type="text" id="editNameInput" defaultValue={this.inputEditCurrentUser}
                                       onBlur={(event) => this.saveNewUserName(event)}/>
                            }
                            <input className="button-white" type="button" onClick={() => this.logout()} value="Выйти"/>
                        </div>
                    }

                </header>

                <header className={'mobile'}>
                    <div>
                        <div className="App-logo">
                            <img src={logo} alt="logo"/>
                            <p>Видеосервис</p>
                        </div>
                        {
                            !this.state.currentUser &&
                            <div className="red-button" onClick={() => this.showLoginForm()}>
                                <input type="button" value="Войти"/>
                            </div>}
                        {
                            this.state.currentUser &&
                            <div className="user">
                                {
                                    !this.state.editUserName &&
                                    <div onClick={() => this.showEditUserName()}>{this.state.currentUser}</div>
                                }
                                {
                                    this.state.editUserName &&
                                    <input type="text" id="editNameInput" defaultValue={this.inputEditCurrentUser}
                                           onBlur={(event) => this.saveNewUserName(event)}/>
                                }
                                <input className="button-white" type="button" onClick={() => this.logout()}
                                       value="Выйти"/>
                            </div>
                        }
                    </div>
                    <div className="search-field">
                        <input type="text" placeholder="Поиск..." onChange={(event) => this.searchStringInput(event)}/>
                        <input type="button" className="button-white" onClick={(event) => this.findElements(event)}
                               value="Найти"/>
                    </div>

                </header>

                {this.state.selectedFilm &&
                <FilmsDetail
                    return={this.returnToFilms}
                    publishCommentary={this.addCommentary}
                    removeCommentary={this.removeCommentary}
                    film={this.state.selectedFilm}
                    author={this.state.currentUser}
                    showError={this.state.commentaryNotValid}
                />
                }

                {!this.state.selectedFilm &&
                <nav>
                    <div className={this.state.showFilms ? "selected" : ""} onClick={() => this.showFilms()}>
                        <strong>Фильмы</strong>
                    </div>
                    <div className={!this.state.showFilms ? "selected" : ""}
                         onClick={() => this.showTv()}><strong>Телеканалы</strong>
                    </div>
                </nav>
                }
                {
                    !this.state.selectedFilm &&
                    currentScreen
                }


                {
                    this.state.showLoginForm &&
                    <div className="login-form">
                        <form>
                            <div className="title">Вход</div>
                            <input type="text" value={this.userLogin} onChange={(event) => this.loginInput(event)}
                                   placeholder="Логин"/>
                            <input type="password" className={this.state.showError ? "error" : ""}
                                   value={this.userPassword} onChange={(event) => this.passwordInput(event)}
                                   placeholder="Пароль"/>
                            <div className="memorize">
                                <label>
                                    <input onClick={(event) => this.setMemorized(event)}
                                           type='checkbox'/>
                                    <span></span> Запомнить меня
                                </label>
                            </div>
                            <div>
                                {
                                    this.state.showError &&
                                    <p>Неверный пароль</p>
                                }
                            </div>
                            <div className="red-button">
                                <input onClick={() => this.login()} type="button" value="Войти"/>
                            </div>
                        </form>
                    </div>
                }
            </div>
        );
    }
}

export default App;
